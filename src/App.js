// import logo from './logo.svg';
// import './App.css';
const gUserInfo = {
  firstname: 'Hoang',
  lastname: 'Pham',
  avatar: 'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png',
  age: 30,
  language: ['Vietnamese', 'Japanese', 'English']
}

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
    <h2>họ và tên user: {gUserInfo.lastname + ' ' + gUserInfo.firstname}</h2>
    <img src={gUserInfo.avatar} width='300px'></img>
    <p>tuổi của user: {gUserInfo.age}</p>
    {gUserInfo.age > 35 ? "anh ấy đã già" : "anh ấy còn trẻ"}
    <ul> ngôn ngữ đã học
      {gUserInfo.language.map((value,index) => {
        return <li>{value}</li>
      })}
    </ul>
    </div>
  );
}

export default App;
